var app = require("express")();
const express = require("express");
var bodyParser = require("body-parser");
require("dotenv").config({ path: ".env" });

var http = require("http").createServer(app);
var io = require("socket.io")(http);

app.use(bodyParser.json());
const Router = express.Router();
Router.route("/post").post(function(req, res) {
  let type = req.body.objectUUID;
  console.log(req.body);
  const ArrayUUID = [req.body.objectUUID, req.body.userUUID];
  if (req.body.objectType === "user") {
    type = ArrayUUID.sort()[0];
  }
  console.log(type);
  io.in(type).emit("new_message", req.body);
  console.log("Message Emitted !");
  res.json("Message Emitted !");
  console.log(req.body);
  return res;
});

app.use("/rabbitmq", Router);

io.on("connection", function(socket) {
  socket.on("join", function(room) {
    socket.join(room);
    console.log("a user joined the room :" + room);
  });
  socket.on("unjoin", function(room) {
    socket.leave(room);
    console.log("a user leave the room :" + room);
  });
  console.log("a user connected");
});
http.listen(
  process.env.NODEJS_SOCKETSERVER_PORT,
  process.env.NODEJS_SOCKETSERVER_HOST,
  function() {
    console.log(
      `listening on ${process.env.NODEJS_SOCKETSERVER_HOST}:${process.env.NODEJS_SOCKETSERVER_PORT}`
    );
  }
);
