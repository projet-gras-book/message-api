from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.filters import BaseFilterBackend
from messageApi.api.serializers import MessageSerializer
from messageApi.api.models import Message

# Create your views here.

## Add parameters to swagger-ui and the openApi schema
class CustomFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        queryset = Message.objects.all()
        objectUUID = request.query_params.get('objectUUID', None)
        objectType = request.query_params.get('objectType',None)
        if objectType is not None and objectUUID is not None:
            queryset = queryset.filter(objectUUID=objectUUID,objectType = objectType)
        return queryset

    def get_schema_operation_parameters(self, view):
        return  [
            {
                'name': 'objectUUID',
                'required': False,
                'in': 'query',
                'schema': {
                    'type': 'string',
                },
            },
            {
                'name': 'objectType',
                'required': False,
                'in': 'header',
                'schema': {
                    'type': 'string',
                },
            },
        ]

class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    filter_backends = [CustomFilter]
    def get_queryset(self):
        pass

    """
    def get_queryset(self):
        queryset = Message.objects.all()
        objectUUID = self.request.query_params.get('objectUUID', None)
        objectType = self.request.query_params.get('objectType',None)
        if objectType is not None and objectUUID is not None:
            queryset = queryset.filter(objectUUID=objectUUID,objectType = objectType)
        return queryset
    """

