from rest_framework import serializers
from messageApi.api.models import Message
import pika
import json
from django.core.serializers.json import DjangoJSONEncoder

class MessageSerializer(serializers.Serializer):
    content = serializers.CharField(required=True,allow_blank=False,max_length=250)
    mediaUrl = serializers.URLField(required=False,allow_blank=True)
    postDate = serializers.DateField()
    userUUID = serializers.UUIDField(required=True)
    objectUUID = serializers.UUIDField(required=True)
    objectType = serializers.CharField(required=True)
    
    def create(self,validated_data):
        ## Data is validated so this data can be save in database
        ## But this cannot avoid issue due to a connection to the database.
        ## if so the message will be send to the socket server but not save in database.
        credentials = pika.PlainCredentials('admin', 'admin')
        connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.126.131',5672,credentials=credentials))
        channel = connection.channel()
        channel.exchange_declare(exchange='message',exchange_type='fanout')
        result = channel.queue_declare(queue='',exclusive=True)
        channel.queue_bind(exchange='message',queue=result.method.queue)
        channel.basic_publish(exchange='message',
                      routing_key='testQueue',
                      body=json.dumps(validated_data, cls=DjangoJSONEncoder))
        connection.close()
        return Message.objects.create(**validated_data)

    def update(self,instance,validated_data):
        instance.id = validated_data.get('id',instance.id)
        instance.content = validated_data.get('content',instance.content)
        instance.mediaUrl = validated_data.get('mediaUrl',instance.mediaUrl)
        instance.postDate = validated_data.get('postDate',instance.postDate)
        instance.userUUID = validated_data.get('userUUID',instance.userUUID)
        instance.objectUUID = validated_data.get('objectUUID',instance.objectUUID)
        instance.objectType = validated_data.get('objectType',instance.objectType)
        return instance;
        