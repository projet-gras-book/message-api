from django.contrib import admin

# Register your models here.
from messageApi.api.models import Message

admin.site.register(Message)