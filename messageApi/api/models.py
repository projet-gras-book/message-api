from django.db import models
from uuid import uuid4

# Create your models here.
class Message(models.Model):
    GROUP = "group"
    USER = "user"
    OBJECT_TYPE = [
        (GROUP, 'group'),
        (USER, 'user'),
    ]

    id = models.UUIDField(primary_key=True,default=uuid4,editable=False)
    content = models.CharField(max_length=250,default='DefaultContent')
    mediaUrl = models.URLField(default='')
    postDate = models.DateField(auto_now=True)
    userUUID = models.UUIDField()
    objectUUID = models.UUIDField()
    objectType = models.CharField(max_length=50,choices=OBJECT_TYPE,default=USER)
    class Meta:
        ordering = ['postDate']

