# Generated by Django 2.2.5 on 2019-10-26 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.UUIDField(primary_key=True, serialize=False)),
                ('content', models.CharField(default='DefaultContent', max_length=250)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
    ]
