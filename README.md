# TODO

[X] Add var env (connection to rabbitmq server, socketserver port, ip ...)
[X] Create docker-compose.yml file in order to coordonate the node server (socketIO), the consumer (python (pika ---> rabbitmq) and the API (python django))

# VAR ENV

| **NAME**                 | **DESCRIPTION**                             | **TYPE** |
| ------------------------ | ------------------------------------------- | -------- |
| NODEJS_SOCKETSERVER_PORT | The port of the socketIO server             | INT      |
| NODEJS_SOCKETSERVER_HOST | The host of your socketIO server            | STRING   |
| DB_NAME                  | Your database's name on the database server | STRING   |
| DB_PORT                  | The port of the database server             | INT      |
| DB_HOST                  | The host of the database server             | STRING   |
| DB_USER                  | The admin user of your database             | STRING   |
| DB_PASSWORD              | The admin's password                        | STRING   |
| RABBITMQ_PORT            | The port of the rabbitmq server             | INT      |
| RABBITMQ_HOST            | The host of the rabbitmq server             | STRING   |
| RABBITMQ_USER            | The admin user of your the rabbitmq         | STRING   |
| RABBITMQ_PASSWORD        | The admin user's password of the rabbitmq   | STRING   |
| CORS_ALL                 | Whether or not the api should allow all CORS| BOOLEAN  |
| CORS_WHITELIST           | Allowed URLs for CORS (ex: http://X:X,...)  | STRING   |
| ALLOWED_HOSTS_ENV        | Allowed IPs for HOST (ex: X.X.X.X,Y.Y.Y.Y)  | STRING   |
| REQUEST_TARGET_PORT      | The port of the socket server for consumer  | INT      |
| REQUEST_TARGET_HOST      | The host of the socket server for consumer  | STRING   |


# PROJECT SETUP (FOR COMPLETE REPRODUCTION)

## NO FRENCH TUTORIAL PROVIDED

## NO GOOD ENGLISH TURORIAL PROVIDED

First of all, make sure to have a recent version of python on your fucking device! (Something like `Python 3.7.2`)
Also beware the blyat! You must use pip through this tutorial an no other package manager like conda the following may not work proprely.
You may also want to upgrade pip to the latest version. You can do so with:

```
python -m pip install --upgrade pip
```

So now got to your project folder (A blank one ok!) and launch a new env for your API with this command:

```
python -m venv env
```

And run the active script:

```
source env/bin/activate  # On Windows use `env\Scripts\activate` (#copy-paste)
```

Then install all packages needed:

```
pip install -r requirements.txt
```

PS: for swagger there is a `rest-framework-swagger` but it seems that he is only based on CoreAPI and not the latest version of OpenAPI. So we will do it manually.

## Project initialisation

So the following command will allow Python to create a new project and an app within it:

```
django-admin startproject messageApi . #☣️☣️☣️ Pay attention to the dot.
cd messageApi
django-admin startapp api
```

## Database initialisation (Optionnal)

This section will create a default database provide by `django`. The database is a SQLite database, so this will just be for testing OK! ✔️ (And don't use MD5 (❌) plz only plain text (✔️).)
Furthermore, this creates two useless tables (user and group), that will only be worth testing.
To initialise the db run:

```
python manage.py migrate
```

If you want to continue the basic tutorial of `django-rest-framework`, continue here, were we left:
https://www.django-rest-framework.org/tutorial/quickstart/

## Basic Empty Entity Setup

Knowing that some of you want to practice Python by themselves, we will only set up an entity with an id and a content in order to add the swagger (after that everything will be up to you CYKA BLYAT 😈).

### Model Set up

Let's create a model, for that you will need to open an IDE, like notepad (😂) and to open the file in messageApi/api/models.py.
In here, write this code:

```
# Create your models here.
class artist(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=250,default='DefaultName')
    picture = models.URLField(verbose_name="picture_url")
    nb_fan = models.IntegerField()
    nb_album = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['id']
```

You may have figured out that the generated architecture of the project isn't based on the principle of modules, like we start doing in the node JS APIs. But, in fact we dont fucking care because this API will only have one table! (eheh 😏)

### Serializers Set up

Now let's move to the serializer, that will allow use to declare basic action and to control the data that will be sent. For that, you must create a `serialisers.py` file (obviously), and write that:

```
from rest_framework import serializers
from messageApi.api.models import Message

class MessageSerializer(serializers.Serializer):
    id = serializers.UUIDField(required=True)
    content = serializers.CharField(required=True,allow_blank=False,max_length=250)
    def create(self,validated_data):

        return Message.objects.create(**validated_data)

    def update(self,instance,validated_data):
        instance.id = validated_data.get('id',instance.id)
        instance.content = validated_data.get('content',instance.content)
```

### View Set up

To finish the basic set up of the entity itself, just add a view in the `views.py` file, like this:

```
from django.shortcuts import render
from rest_framework import viewsets
from messageApi.api.serializers import MessageSerializer
from messageApi.api.models import Message

# Create your views here.
class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
```

## Set up Swagger and App configration

We now have a model! But we don't know if it works and how we can test it (manually)... ❓❓❓

In order to test the model, we will run the app, but we must configure it before.

### Model registration

Move to the `admin.py` file that will contain all our model that we want to expose. And register the model:

```
from messageApi.api.models import Message

admin.site.register(Message)
```

### Urls configuration

We also have to create a router and his URLs as well as register the view, in order to display it through HTTP request. Open the `urls.py` (in messageApi folder):

```
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from messageApi.api import views

router = routers.DefaultRouter()
router.register(r'messages', views.MessageViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
]

```

### Global configuration

Our routes are now created, but this will not work either as we haven't configured 🔧🔧🔧 the server to install our app. To do so, open the `settings.py` file and register rest-framework and our app. Your INSTALLED_APPS array should look like this:

```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'messageApi.api'
]
```

### Create Migration

We have a superb SQLite database but sadly, it smells shit and it's totally useless, but not for long! We are going to create a migration now that everything is in order.
Open a command shell that work (preferably), and move to your `manage.py` folder and run this command:

```
python manage.py makemigrations api
```

We will see this if it works (hopefully):

```
Migrations for 'api':
  messageApi\api\migrations\0001_initial.py
    - Create model Message
```

If it works, you can now run this:

```
python manage.py migrate
```

And see if it works:

```
Running migrations:
  Applying api.0001_initial... OK
```

Now you can run your server ("YEAH !!! 👏👏👏"), with this:

```
python manage.py runserver 0.0.0.0:8580
```

And if everything is okay go to http://localhost:8580/messages/ to insert message in your SQLite db.

### Set up OpenAPI schema and it's Swagger

First of all, it seems like there are hundred of solutions to generate a documentation. I chose to use OpenApi and swagger. if I remember correctly we chose it for our docs (And if don't eheh we are idiots!).
So in order to generate this doc, we will have to create a dynamic OpenAPI schema (which will updates him self as we change our entity). To do so, go to the `urls.py` file and add a route:

```
from rest_framework.schemas import get_schema_view

urlpatterns = [
    # ...
    path('openapi', get_schema_view(
        title="Message Api",
        description="README OF BLYAT",
        version="1.0.0"
    ), name='openapi-schema'),
    # ...
]
```

Once you have done that, you must create a new template.
⚠️⚠️⚠️⚠️⚠️⚠️ You must place the template in the api/templates or else django will not be able to find it ⚠️⚠️⚠️⚠️⚠️⚠️

Create the file `swagger-ui.html` in the corresponding folder.
And this:

```
<!DOCTYPE html>
<html>
  <head>
    <title>Swagger</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
  </head>
  <body>
    <div id="swagger-ui"></div>
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <script>
    const ui = SwaggerUIBundle({
        url: "{% url schema_url %}",
        dom_id: '#swagger-ui',
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIBundle.SwaggerUIStandalonePreset
        ],
        layout: "BaseLayout"
      })
    </script>
  </body>
</html>
```

Then we will have to register the template to django and provided the OpenAPI schema generated to our template. To do this, we will add this route to the `urls.py`:

```
from django.views.generic import TemplateView

urlpatterns = [
    # ...`SchemaView`.
    path('swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi-schema'}
    ), name='swagger-ui'),
```

Now if you run your server and go to http://localhost:8580/swagger-ui/, you should this the swagger!
PS: if you want to get the OpenAPI schema, go to http://localhost:8580/openapi!!

We are now done. Fortunately, I was fed up with it (in other word, FUCK IT CYKA BLYAT 🖕🖕🖕🖕).

# Other solution

Just clone this repos retard... (eheh boy SPICY MEAT 🌶️🌶️🌶️). Don't forget to set up the virtual env, which is recommanded by Django with:

```
python -m venv env
```

# USEFULL SOURCE

I don't invent anything so if you want to now more go to those links CYKA BLYAT!

https://www.django-rest-framework.org/tutorial/1-serialization/#creating-a-model-to-work-with
https://www.django-rest-framework.org/topics/documenting-your-api/#generating-documentation-from-openapi-schemas
https://www.django-rest-framework.org/api-guide/schemas/
