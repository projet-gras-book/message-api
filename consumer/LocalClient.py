import asyncio
import multiprocessing
import time
import socketio


listmsgRabbit = []

loop = asyncio.get_event_loop()
sio = socketio.AsyncClient(logger=True, engineio_logger=True)
start_timer = None


async def send_ping():
    global start_timer
    start_timer = time.time()
    await sio.emit('join','f9990de3-30f7-4faa-9e23-15bcc75e19d1')

@sio.on('connect')
async def on_connect():
    print('connected to server')
    await send_ping()

@sio.on('pong_from_server')
async def on_pong(data):
    global start_timer
    latency = time.time() - start_timer
    print('latency is {0:.2f} ms'.format(latency * 1000))
    await sio.sleep(1)
    await send_ping()

@sio.on('new_message')
async def on_message(data):
    print('New message received !')

async def start_server():
    await sio.connect('http://localhost:7894')
    await sio.wait()


loop.run_until_complete(start_server())
