import pika
import requests
import requests.sessions
import json
from dotenv import load_dotenv
load_dotenv()
from pathlib import Path 
env_path = Path('../') / '.env'
import os
RABBITMQ_USER = os.getenv("RABBITMQ_USER")
RABBITMQ_PASSWORD = os.getenv("RABBITMQ_PASSWORD")
RABBITMQ_HOST = os.getenv("RABBITMQ_HOST")
RABBITMQ_PORT = os.getenv("RABBITMQ_PORT")
REQUEST_TARGET_HOST = os.getenv("REQUEST_TARGET_HOST")
REQUEST_TARGET_PORT = os.getenv("REQUEST_TARGET_PORT")


API_ENDPOINT = "http://"+REQUEST_TARGET_HOST+":"+REQUEST_TARGET_PORT+"/rabbitmq/post"


listmsgRabbit = []
def callback(ch, method, properties, body):
    listmsgRabbit.append(body)
    print(" [x] Received %r" % body)
    s = requests.session()
    s.keep_alive = False
    print("Forwarding of the response to the socket server on "+API_ENDPOINT)
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = s.post(url = API_ENDPOINT, json = json.loads(body.decode("utf-8"))) 
    s.close()
  
    # extracting response text  
    pastebin_url = r.json() 
    print("Response of the socket server :"+ pastebin_url)

credentials = pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASSWORD)
connection = pika.BlockingConnection(pika.ConnectionParameters(RABBITMQ_HOST,RABBITMQ_PORT,credentials=credentials))
channel = connection.channel()
channel.exchange_declare(exchange='message', exchange_type='fanout')

result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='message', queue=queue_name)
channel.basic_consume(
queue=queue_name, on_message_callback=callback, auto_ack=True)
print("Start consumming from "+REQUEST_TARGET_HOST)
channel.start_consuming()